package eu.cloudlightning.bcri.blueprint;

public class Resources {
    private String resourceDescriptor;
    private String resourceCreationId;

    public Resources() {
	this.resourceDescriptor = "";
	this.resourceCreationId = "";
    }

    /**
     * @return the resourceDescriptor
     */
    public String getResourceDescriptor() {
	return resourceDescriptor;
    }

    /**
     * @param resourceDescriptor
     *            the resourceDescriptor to set
     */
    public void setResourceDescriptor(String resourceDescriptor) {
	this.resourceDescriptor = resourceDescriptor;
    }

    /**
     * @return the resourceCreationId
     */
    public String getResourceCreationId() {
	return resourceCreationId;
    }

    /**
     * @param resourceCreationId
     *            the resourceCreationId to set
     */
    public void setResourceCreationId(String resourceCreationId) {
	this.resourceCreationId = resourceCreationId;
    }

}
