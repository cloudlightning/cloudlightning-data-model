import com.fasterxml.jackson.databind.ObjectMapper;
import eu.cloudlightning.bcri.blueprint.*;
import junit.framework.TestCase;

import java.io.File;
import java.io.IOException;

/**
 * Created by adrian on 20.01.2017.
 */
public class BlueprintModelTest extends TestCase {

    public void testResourceTemplate() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        BlueprintResourceTemplate json = mapper.readValue(new File("src/test/resources/BlueprintResourceTemplate.json"), BlueprintResourceTemplate.class);
        assertNotNull(json);

        assertNotNull(json.getBlueprintId());
        assertNotNull(json.getCallbackEndpoint());
        assertNotNull(json.getCost());
        assertNotNull(json.getServiceElements());
        for(ServiceElement se : json.getServiceElements()){
            assertNotNull(se);
            assertNotNull(se.getServiceElementId());
            assertNotNull(se.getImplementations());
            for(Implementation i : se.getImplementations()){
                assertNotNull(i);
                assertNotNull(i.getAcceleratorRange());
                assertNotNull(i.getBandwidthRange());
                assertNotNull(i.getComputationRange());
                assertNotNull(i.getImplementationType());
                assertNotNull(i.getMemoryRange());
                assertNotNull(i.getStorageRange());
            }
        }

    }

    public void testResourcedTemplate() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        BlueprintResourcedTemplate template = mapper.readValue(new File("src/test/resources/BlueprintResourcedTemplate2.json"), BlueprintResourcedTemplate.class);
        assertNotNull(template);
        assertNotNull(template.getBlueprintId());
        assertNotNull(template.getCallbackEndpoint());
        assertNotNull(template.getStatus());
        assertNotNull(template.getResourcedServiceElements());

        for(ResourcedServiceElement rse : template.getResourcedServiceElements()){
            assertNotNull(rse);
            assertNotNull(rse.getStatus());
            assertNotNull(rse.getServiceElementId());
            assertNotNull(rse.getImplementationType());
            assertNotNull(rse.getCreatorId());
            assertNotNull(rse.getResourceType());
            assertNotNull(rse.getResources());
            for(Resources r : rse.getResources()){
                assertNotNull(r);
                assertNotNull(r.getResourceCreationId());
                assertNotNull(r.getResourceDescriptor());
            }
        }
    }
}
