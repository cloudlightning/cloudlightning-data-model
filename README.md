# README #

This repository shares common resources to be used in the CloudLightning components. 

## Usage ##
The CloudLightning data model is intended to be used as a Maven dependency, but it can also be used as a standalone library. 

### Prerequisites ###
* Maven

#### Use as dependency ####
1. Change into the root directory and execute ```mvn install```
2. Add the maven dependency in your project:

```
#!xml

<dependency>
    <groupId>eu.cloudlightning.core</groupId>
    <artifactId>data-model</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

#### Use as standalone library ####
1. Change into the root directory and execute ```mvn package```
2. Use the jar generated in ```target/data-model-1.0-SNAPSHOT.jar``` in your project.